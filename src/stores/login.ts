import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMassageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const messageStore = useMassageStore();
  const userStore = useUserStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });
  const login = (username: string, password: string): void => {
    if (userStore.login(username, password)) {
      loginName.value = username;
      localStorage.setItem("loginName", username);
    } else {
      messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง");
    }
  };
  const logout = (): void => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
